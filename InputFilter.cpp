/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "InputFilter.h"
#include "Machine.h"
#include "AtariJoystick.h"
#include <QKeyEvent>

InputFilter::InputFilter(Machine *machine, QObject *parent) :
QObject(parent),
myMachine(machine)
{
}

bool InputFilter::eventFilter(QObject *obj, QEvent *event)
{
	QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

	if(event->type() == QEvent::KeyPress)
	{
		if(keyEvent->isAutoRepeat())
			return true;

		switch(keyEvent->key())
		{
			case Qt::Key_K:
				myMachine->toggleDebugMode();
				break;
			case Qt::Key_A:
				myMachine->joystick(0)->moveLeft();
				break;
			case Qt::Key_D:
				myMachine->joystick(0)->moveRight();
				break;
			case Qt::Key_S:
				myMachine->joystick(0)->moveDown();
				break;
			case Qt::Key_W:
				myMachine->joystick(0)->moveUp();
				break;
			case Qt::Key_Space:
				myMachine->joystick(0)->pressButton();
				break;
			case Qt::Key_F2:
				myMachine->reset(true);
				break;
			case Qt::Key_F1:
				myMachine->select(true);
				break;
			case Qt::Key_M:
				myMachine->togglePlayerDifficulty(0);
				break;
		}
		return true;
	}
	else if(event->type() == QEvent::KeyRelease)
	{
		if(keyEvent->isAutoRepeat())
			return true;

		switch(keyEvent->key())
		{
			case Qt::Key_A:
				myMachine->joystick(0)->releaseLeft();
				break;
			case Qt::Key_D:
				myMachine->joystick(0)->releaseRight();
				break;
			case Qt::Key_S:
				myMachine->joystick(0)->releaseDown();
				break;
			case Qt::Key_W:
				myMachine->joystick(0)->releaseUp();
				break;
			case Qt::Key_Space:
				myMachine->joystick(0)->releaseButton();
				break;
			case Qt::Key_F2:
				myMachine->reset(false);
				break;
			case Qt::Key_F1:
				myMachine->select(false);
				break;
		}
	}

	return QObject::eventFilter(obj, event);
}
