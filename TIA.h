/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef TIA_H
#define TIA_H

#include "DataBus.h"

class TIAVideoRenderer;
class AtariJoystick;

class TIA
{
public:
	enum TelevisionSystem {
		NtscSystem,
		PalSystem,
		SecamSystem
	};

public:
	TIA();
	bool				attach(DataBus *dataBus, Address start, Address end);
	void				setVideoRenderer(TIAVideoRenderer *videoRenderer);
	void				setJoysticks(AtariJoystick* joystick0, AtariJoystick* joystick1);
	void				run(int clocks) { update(clocks); }
	void				reset();
	bool				currentFrameFinished();
	TelevisionSystem televisionSystem() const { return myTelevisionSystem; }

private:
	enum { TIAMemorySize = 0x40 };

	struct FrameTiming {
		quint16 vblankEdge;
		quint16 overScanEdge;
	};
	static const FrameTiming	FrameTimings[SecamSystem+1];
	TelevisionSystem					myTelevisionSystem;

	enum WriteAddress {
		VSYNC = 0x00,	VBLANK = 0x01, WSYNC = 0x02, RSYNC = 0x03, NUSIZ0 = 0x04, NUSIZ1 = 0x05, COLUP0 = 0x06, COLUP1 = 0x07,
		COLUPF = 0x08, COLUBK = 0x09, CTRLPF = 0x0A, REFP0 = 0x0B, REFP1 = 0x0C, PF0 = 0x0D, PF1 = 0x0E, PF2 = 0x0F, RESP0 = 0x10,
		RESP1 = 0x11, RESM0 = 0x12, RESM1 = 0x13, RESBL = 0x14, AUDC0 = 0x15,	AUDC1 = 0x16,	AUDF0 = 0x17,	AUDF1 = 0x18,	AUDV0 = 0x19,
		AUDV1 = 0x1A,	GRP0 = 0x1B, GRP1 = 0x1C,	ENAM0 = 0x1D,	ENAM1 = 0x1E,	ENABL = 0x1F,	HMP0 = 0x20, HMP1 = 0x21,	HMM0 = 0x22,
		HMM1 = 0x23, HMBL = 0x24, VDELP0 = 0x25, VDELP1 = 0x26,	VDELBL = 0x27, RESMP0 = 0x28,	RESMP1 = 0x29, HMOVE = 0x2A, HMCLR = 0x2B,
		CXCLR = 0x2C
	};

	enum ReadAddress {
		CXM0P = 0x00	,	CXM1P = 0x01,	CXP0FB = 0x02	, CXP1FB = 0x03	,	CXM0FB = 0x04	, CXM1FB = 0x05	, CXBLPF = 0x06,
		CXPPMM = 0x07	, INPT0 = 0x08, INPT1 = 0x09	, INPT2 = 0x0A	, INPT3 = 0x0B	, INPT4 = 0x0C	, INPT5 = 0x0D
	};

	Byte			myMemory[TIAMemorySize];
	DataBus*	myDataBus;

	/* Video renderer */
	TIAVideoRenderer* myVideoRenderer;

	quint16		myCurrentScanlineClocks;
	quint16		myCurrentScanline;
	bool			myCurrentFrameFinished;

	/* Cartridge type auto-detection */
	quint16		myPalFrames;
	bool			myDetectingTimingsFlag;
	quint8		myDetectTimingsFrame;
	qint16		myHBlankLines;

	/* Joysticks */
	AtariJoystick*	myJoystick0;
	AtariJoystick*	myJoystick1;
	bool						myDumpedInputPortsFlag;
	quint8					myDumpedPort0Charge;
	quint8					myDumpedPort1Charge;

	/* Background */
	struct Background {
		quint8 color;
		quint8 luminosity;
	};
	Background myBackground;

	/* Playfield */
	struct Playfield {
		bool hasPriorityOverPlayers;
		bool score;
		bool reflect;
		quint8 color;
		quint8 luminosity;
		quint32 playfieldBits;
	};
	Playfield myPlayfield;

	/* Movable Objects */
	struct MovableObject {
		qint8		hmm;
		qint16	position;
		quint8	scanIndex;					// pixel scan position
		quint8	scanSize;						// pixel scan size
		quint8	scanShift;					// bit shift for scan position bit index
		quint8	scanDelay;

		bool		resetWrittenFlag;		// object RESXX latch was written
		quint8  color;
		quint8  luminosity;
	};

	enum PlayerCopies {
		OneCopy,
		TwoCopiesClose,
		TwoCopiesMed,
		ThreeCopiesClose,
		TwoCopiesWide,
		OneDoubleSizedCopy,
		ThreeCopiesMed,
		OneQuadSizedCopy
	};

	struct Player: public MovableObject {
		quint8				grp;
		quint8				oldGrp;
		PlayerCopies	playerCopies;
		bool					verticalDelay;
		bool					reflect;
	};

	struct Ball: public MovableObject {
		bool		verticalDelay;
		bool		enabled;
		bool		oldEnabled;
	};

	struct Missile: public MovableObject {
		bool		enabled;
		bool		setToEnabled; //this indicates whether ENAMx was called trying to set enabled to 1
		bool		lockedOnPlayer;
	};

	Player	myPlayers[2];
	Ball		myBall;
	Missile myMissiles[2];
	bool		myLateResetHBLANKFlag;

	/* Collision detection */
	enum ObjectsMasks {
		Player0Mask  = 0x01,
		Player1Mask  = 0x02,
		Missile0Mask = 0x04,
		Missile1Mask = 0x08,
		BallMask		 = 0x10,
		PlayfieldMask= 0x20
	};
	Byte myCollisions;
	Byte myCXM0P, myCXM1P, myCXP0FB, myCXP1FB, myCXM0FB, myCXM1FB, myCXBLPF, myCXPPMM;

	/* Sound */
	enum NoiseContent {
		NoiseSetTo1		,	Noise4BitPoly	, NoiseDiv15_4BitPoly	, Noise5BitPoly_4BitPoly,
		NoiseDiv2Pure0,	NoiseDiv2Pure1, NoiseDiv31Pure0			, Noise5BitPolyDiv2,
		Noise9BitPoly	, Noise5BitPoly	, NoiseDiv31Pure1			, NoiseSetLast4BitsTo1,
		NoiseDiv6Pure0, NoiseDiv6Pure1, NoiseDiv93Pure			,	Noise5BitPolyDiv6
	};

	quint8				myAudioDivisor0, myAudioDivisor1;
	quint8				myAudioVolume0 , myAudioVolume1;
	NoiseContent	myAudioContent0, myAudioContent1;

	/* Main */
	void				update(int clocks);
	void				startFrame();
	void				endFrame();

	/* Objects movement */
	void				doHMove();
	void				resetObjectPosition(MovableObject* object);
	qint8				readHorizontalMovement(quint8 byte);
	void				wrapAround(qint16* pos);

	/* Drawing */
	void				triggerDrawingObjects();
	bool				drawingActive(MovableObject* object);
	void				draw();
	void				drawBackground();
	void				drawPlayField();
	void				drawPlayer(int playerNum);
	void				drawMissile(int missileNum);
	void				drawBall();
	void				updateScanIndex();
	void				getBeamPosition(quint8 *x, quint16 *y) const;
	void				setPixel(quint8 x, quint16 y, bool draw, quint8 color, quint8 luminosity);

	/* Collision detection */
	void				updateCollisionLatches();
	void				clearCollisionLatches();

	/* Sound */
	void				updateSound() { /* TODO */ }

	/* DataBus */
	static Byte read(void *object, Address address);
	static void write(void *object, Address address, Byte value);

	void				debug(const QString& debugMessage);

};

#endif // TIA_H
