/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef M6532_H
#define M6532_H

#include "DataBus.h"

class M6532
{
public:
	M6532();
	~M6532();

	void reset();
	void run(int clks);
	void setDataBus(DataBus *dataBus) { myDataBus = dataBus; }
	bool attachRam(Address start, Address end);
	bool attachPortsAndTimer(Address start, Address end);

private:
	static Byte readRam(void *object, Address address);
	static void writeRam(void *object, Address address, Byte value);

	static Byte readPort(void *object, Address address);
	static void writePort(void *object, Address address, Byte value);

	enum {
		RamMemorySize = 0x80,
		PortsMemorySize = 0x20
	};

	enum TimerType {
		TIM1T = 0x14,
		TIM8T = 0x15,
		TIM64T = 0x16,
		T1024T = 0x17,
		INTIM = 0x04
	};

	static const quint8 ShiftTable[4];

	DataBus*		myDataBus;

	// RIOT's RAM 128K space
	Byte				myRam[RamMemorySize];
	// RIOT's port and internal timer
	Byte				myPorts[PortsMemorySize];

	bool				myIRQEnabled, myIRQTriggered;
	quint8			myTimerShift;
	quint32			myTimerClocks;
	quint32			myClocksSinceTimerStarted;
};

#endif // M6532_H
