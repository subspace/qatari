 /*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "TIA.h"
#include "TIAVideoRenderer.h"
#include "AtariJoystick.h"

#include <QString>
#include <qmath.h>
#include <memory.h>
#include "M6502.h"

#define DEBUGBUG
#undef DEBUGBUG

const TIA::FrameTiming TIA::FrameTimings[SecamSystem+1] = {
{ 40, 40+192 },
{ 48, 48+228 },
{ 48, 48+228 }
};

TIA::TIA():
myDataBus(NULL),
myVideoRenderer(NULL)
{
	reset();
}

bool TIA::currentFrameFinished()
{
	if(myCurrentFrameFinished)
	{
		myCurrentFrameFinished = false;
		return true;
	}
	return false;
}

void TIA::setVideoRenderer(TIAVideoRenderer *videoRenderer)
{
	myVideoRenderer = videoRenderer;
}

void TIA::getBeamPosition(quint8 *x, quint16 *y) const
{
	*x = myCurrentScanlineClocks - 68;
	*y = myCurrentScanline;
}

void TIA::drawBackground()
{
	quint8	x;
	quint16 y;

	getBeamPosition(&x, &y);

	myVideoRenderer->drawPixel(x, y, myBackground.color, myBackground.luminosity);
}

void TIA::drawPlayField()
{
	quint8	x;
	quint16 y;
	quint8 color, luminosity;

	getBeamPosition(&x, &y);

	quint8 bitIndex;

	/* Left side of the screen */
	if(x < 80)
	{
		if(myPlayfield.score)
		{
			color = myPlayers[0].color;
			luminosity = myPlayers[0].luminosity;
		}
		else
		{
			color = myPlayfield.color;
			luminosity = myPlayfield.luminosity;
		}

		bitIndex = x >> 2;
		if((myPlayfield.playfieldBits >> (19-bitIndex)) & 1)
		{
			myVideoRenderer->drawPixel(x, y, color, luminosity);
			myCollisions |= PlayfieldMask;
		}
		return;
	}

	/* Right side of the screen */
	if(myPlayfield.score)
	{
		color = myPlayers[1].color;
		luminosity = myPlayers[1].luminosity;
	}
	else
	{
		color = myPlayfield.color;
		luminosity = myPlayfield.luminosity;
	}

	bitIndex = (x-80)>>2;
	if(!myPlayfield.reflect)
	{
		if((myPlayfield.playfieldBits >> (19-bitIndex)) & 1)
		{
			myVideoRenderer->drawPixel(x, y, color, luminosity);
			myCollisions |= PlayfieldMask;
		}
	}
	else
	{
		if((myPlayfield.playfieldBits >> bitIndex) & 1)
		{
			myVideoRenderer->drawPixel(x, y, color, luminosity);
			myCollisions |= PlayfieldMask;
		}
	}
}

bool TIA::drawingActive(MovableObject* object)
{
	/* check scan position */
	if(object->scanIndex >= object->scanSize && object->scanIndex != 0xff)
	{
		object->scanIndex = 0xff;
		return false;
	}

	/* check delay */
	if(!object->scanDelay)
		return false;
	else
	{
		object->scanDelay--;
		return true;
	}
	return false;
}

void TIA::drawPlayer(int playerNum)
{
	Player*	player = &myPlayers[playerNum];

	if(drawingActive(player))
		return;

	if(player->scanIndex >= player->scanSize)
	{
		player->scanIndex = 0xff;
		return;
	}

	quint8	x;
	quint16 y;
	Byte		mask;
	quint8	grp;

	if(playerNum)
		mask = Player1Mask;
	else
		mask = Player0Mask;

	/*
	Vertical Delay bit - this is also read every time a pixel is
	generated and used to select which of the "new" (0) or "old" (1)
	Player Graphics registers is used to generate the pixel. (ie
	the pixel is retrieved from both registers in parallel, and
	this flag used to choose between them at the graphics output).
	It is safe to modify VDELPn at any time, with immediate effect.
	*/
	if(player->verticalDelay)
		grp = player->oldGrp;
	else
		grp = player->grp;

	getBeamPosition(&x, &y);

	if(!playerNum)
		debug("draw " + QString::number(player->scanIndex >> player->scanShift));

	if(!player->reflect)
	{
		if(grp & (0x80 >> (player->scanIndex >> player->scanShift)))
		{
			myVideoRenderer->drawPixel(x, y, player->color, player->luminosity);
			myCollisions |= mask;
		}
	}
	else
	{
		if(grp & (0x01 << (player->scanIndex >> player->scanShift)))
		{
			myVideoRenderer->drawPixel(x, y, player->color, player->luminosity);
			myCollisions |= mask;
		}
	}
}

void TIA::drawMissile(int missileNum)
{
	Missile *missile = &myMissiles[missileNum];

	if(drawingActive(missile))
		return;

	if(missile->scanIndex >= missile->scanSize)
	{
		missile->scanIndex = 0xff;
		return;
	}
	if(!missile->enabled || missile->lockedOnPlayer)
		return;

	Byte mask;
	if(missileNum)
		mask = Missile1Mask;
	else
		mask = Missile0Mask;

	quint8	x;
	quint16 y;
	getBeamPosition(&x, &y);

	myVideoRenderer->drawPixel(x, y, missile->color, missile->luminosity);
	myCollisions |= mask;
}

void TIA::drawBall()
{
	if(drawingActive(&myBall))
		return;

	if(myBall.scanIndex >= myBall.scanSize)
	{
		myBall.scanIndex = 0xff;
		return;
	}

	if(myBall.verticalDelay)
	{
		if(!myBall.oldEnabled)
			return;
	}
	else
	{
		if(!myBall.enabled)
			return;
	}

	quint8	x;
	quint16 y;
	getBeamPosition(&x, &y);

	myVideoRenderer->drawPixel(x, y, myBall.color, myBall.luminosity);
	myCollisions |= BallMask;
}

/*
==========================================================================
draw
---------------------------------------------
Simply draws using painter's algorithm
*/
void TIA::draw()
{
	if(myCurrentScanlineClocks < 68)
		return;

	quint8	x;
	quint16 y;
	getBeamPosition(&x, &y);

	if(myMemory[VBLANK] & 0x02 || myMemory[VSYNC] & 0x02)
	{
		myVideoRenderer->drawPixel(x, y, 0, 0);
		return;
	}

	drawBackground();

	if(!myPlayfield.hasPriorityOverPlayers)
	{
		drawPlayField();
		drawBall();
		drawPlayer(1);
		drawMissile(1);
		drawPlayer(0);
		drawMissile(0);
	}
	else
	{
		drawPlayer(1);
		drawMissile(1);
		drawPlayer(0);
		drawMissile(0);
		drawBall();
		drawPlayField();
	}

	/* Emulate late HBLANK reset period */
	if(myLateResetHBLANKFlag && myCurrentScanlineClocks < 76)
		myVideoRenderer->drawPixel(x, y, 0, 0);
}

void TIA::updateScanIndex()
{
	if(myCurrentScanlineClocks < 68)
		return;

	if(myPlayers[0].scanIndex != 0xff)
		myPlayers[0].scanIndex++;
	if(myPlayers[1].scanIndex != 0xff)
		myPlayers[1].scanIndex++;
	if(myMissiles[0].scanIndex != 0xff)
		myMissiles[0].scanIndex++;
	if(myMissiles[1].scanIndex != 0xff)
		myMissiles[1].scanIndex++;
	if(myBall.scanIndex != 0xff)
		myBall.scanIndex++;
}

void TIA::update(int clocks = 3)
{
	for(int i = 0; i < clocks; ++i)
	{
		triggerDrawingObjects();
		draw();
		updateScanIndex();
		updateCollisionLatches();

		/* update scanline counter */
		myCurrentScanlineClocks++;
		if(myCurrentScanlineClocks == 228)
		{
			myLateResetHBLANKFlag = false;
			myCurrentScanlineClocks = 0;
			myCurrentScanline++;

			if(myDetectingTimingsFlag)
			{
				if(myMemory[VBLANK] & 0x02)
					myHBlankLines++;
			}
			updateSound();
		}
	}
}

bool TIA::attach(DataBus *dataBus, Address start, Address end)
{
	if((end-start)+1 != TIAMemorySize)
		return false;
	myDataBus = dataBus;
	return myDataBus->attach(this, start, end, TIA::read, TIA::write);
}

void TIA::triggerDrawingObjects()
{
	int x = myCurrentScanlineClocks - 68;
	for(int i = 0; i < 2; ++i)
	{
		Player	*player = &myPlayers[i];
		Missile *missile = &myMissiles[i];

		/* player */
		if(((player->position + 1 ) % 160 == x)
		|| ((player->position + 17) % 160 == x && (player->playerCopies == TwoCopiesClose		|| player->playerCopies == ThreeCopiesClose))
		|| ((player->position + 33) % 160 == x && (player->playerCopies == ThreeCopiesClose || player->playerCopies == TwoCopiesMed || player->playerCopies == ThreeCopiesMed))
		|| ((player->position + 65) % 160 == x && (player->playerCopies == ThreeCopiesMed		|| player->playerCopies == TwoCopiesWide)))
		{
			if(player->resetWrittenFlag)
				player->resetWrittenFlag = false;
			else
				player->scanIndex = 0;
		}

		/* missile */
		if(((missile->position + 0 ) % 160 == x)
		|| ((missile->position + 16) % 160 == x && (player->playerCopies == TwoCopiesClose	 || player->playerCopies == ThreeCopiesClose))
		|| ((missile->position + 32) % 160 == x && (player->playerCopies == ThreeCopiesClose || player->playerCopies == TwoCopiesMed || player->playerCopies == ThreeCopiesMed))
		|| ((missile->position + 64) % 160 == x && (player->playerCopies == ThreeCopiesMed	 || player->playerCopies == TwoCopiesWide)))
		{
			if(missile->resetWrittenFlag)
				missile->resetWrittenFlag = false;
			else
				missile->scanIndex = 0;
		}
	}

	/* ball */
	if(myBall.position == x)
		myBall.scanIndex = 0;
}

void TIA::reset()
{
	myCurrentScanlineClocks = 0;
	myCurrentScanline = 0;

	myPalFrames = 0;
	myDetectingTimingsFlag = true;
	myDetectTimingsFrame = 0;
	myTelevisionSystem = NtscSystem;
	if(myVideoRenderer)
		myVideoRenderer->buildNTSCPalette();

	memset(myMemory, 0, 0x40);

	for(int i = 0; i < 2; ++i)
	{
		memset(&myPlayers[i], 0, sizeof(Player));
		myPlayers[i].scanIndex = 0xff;

		memset(&myMissiles[i], 0, sizeof(Missile));
		myMissiles[i].scanIndex = 0xff;
	}
	memset(&myBall, 0, sizeof(Ball));
	myBall.scanIndex = 0xff;

	memset(&myPlayfield, 0, sizeof(Playfield));

	clearCollisionLatches();

	myLateResetHBLANKFlag = false;
}

//========================================================================
// startFrame endFrame
// ---------------------------------------------
//
//
void TIA::startFrame()
{
	myVideoRenderer->startDrawing();

	myCurrentScanlineClocks = 0;
	myCurrentScanline = 0;
}

void TIA::endFrame()
{
	// draw entire video buffer
	if(!myDetectingTimingsFlag)
		myVideoRenderer->endDrawing();

	// detect timings
	if(myDetectingTimingsFlag)
	{
		if(myDetectTimingsFrame < 20)
		{
			qDebug("%i", myHBlankLines);
			if(myHBlankLines > 42)
				myPalFrames++;
			myDetectTimingsFrame++;
			myHBlankLines = 0;
		}
		else
		{
			if(myPalFrames > 10)
			{
				myTelevisionSystem = PalSystem;
				if(myVideoRenderer)
					myVideoRenderer->buildPALPalette();
			}
			else
			{
				myTelevisionSystem = NtscSystem;
				if(myVideoRenderer)
					myVideoRenderer->buildNTSCPalette();
			}
			myDetectingTimingsFlag = false;
		}
	}
	myCurrentFrameFinished = true;
}

/*
==========================================================================
Set the collision latches
---------------------------------------------
TODO: make a table to translate those...
*/
void TIA::updateCollisionLatches()
{
	if(myCollisions & Missile0Mask)
	{
		if(myCollisions & Player1Mask)
			myCXM0P |= 0x80;
		if(myCollisions & Player0Mask)
			myCXM0P |= 0x40;
	}

	if(myCollisions & Missile1Mask)
	{
		if(myCollisions & Player0Mask)
			myCXM1P |= 0x80;
		if(myCollisions & Player1Mask)
			myCXM1P |= 0x40;
	}

	if(myCollisions & Player0Mask)
	{
		if(myCollisions & PlayfieldMask)
			myCXP0FB |= 0x80;
		if(myCollisions & BallMask)
			myCXP0FB |= 0x40;
	}

	if(myCollisions & Player1Mask)
	{
		if(myCollisions & PlayfieldMask)
			myCXP1FB |= 0x80;
		if(myCollisions & BallMask)
			myCXP1FB |= 0x40;
	}

	if(myCollisions & Missile0Mask)
	{
		if(myCollisions & PlayfieldMask)
			myCXM0FB |= 0x80;
		if(myCollisions & BallMask)
			myCXM0FB |= 0x40;
	}

	if(myCollisions & Missile1Mask)
	{
		if(myCollisions & PlayfieldMask)
			myCXM1FB |= 0x80;
		if(myCollisions & BallMask)
			myCXM1FB |= 0x40;
	}

	if((myCollisions & BallMask) && (myCollisions & PlayfieldMask))
		myCXBLPF = 0x80;

	if((myCollisions & Player0Mask) && (myCollisions & Player1Mask))
		myCXPPMM |= 0x80;
	if((myCollisions & Missile0Mask) && (myCollisions & Missile1Mask))
		myCXPPMM |= 0x40;

	myCollisions = 0;
}

void TIA::clearCollisionLatches()
{
	myCollisions = 0;
	myCXM0P  = myCXM1P  = 0;
	myCXP0FB = myCXP1FB = 0;
	myCXM0FB = myCXM1FB = 0;
	myCXBLPF = myCXPPMM = 0;
}

qint8 TIA::readHorizontalMovement(quint8 byte)
{
	qint8 m;
	if(byte & 0x80)
		m = -16 + (byte >> 4);
	else
		m = (byte >> 4);

	return m;
}

//========================================================================

Byte TIA::read(void *object, Address address)
{
	TIA *self = THIS(TIA);

	switch(address & 0x000f)
	{
		case CXM0P:
			return self->myCXM0P;
		case CXM1P:
			return self->myCXM1P;
		case CXP0FB:
			return self->myCXP0FB;
		case CXP1FB:
			return self->myCXP1FB;
		case CXM0FB:
			return self->myCXM0FB;
		case CXM1FB:
			return self->myCXM1FB;
		case CXBLPF:
			return self->myCXBLPF;
		case CXPPMM:
			return self->myCXPPMM;

		case INPT0:
		case INPT1:
		case INPT2:
		case INPT3:
			break;
		case INPT4:
			if(self->myJoystick0->isButtonPressed())
				return 0x00;
			else
				return 0x80;
		case INPT5:
			if(self->myJoystick1->isButtonPressed())
				return 0x00;
			else
				return 0x80;
			break;
	}
	return self->myMemory[address];
}

void TIA::write(void *object, Address address, Byte value)
{
	TIA *self = THIS(TIA);
//	qint16 delay = PokeDelay[address];

//	// See if this is a poke to a PF register
//	if(delay == -1)
//	{
////		static quint32 d[4] = {4, 5, 2, 3};
////		delay = d[(self->myCurrentScanlineClocks / 3) & 3];
//		delay = 0;
//	}
//	self->run(delay);

	switch(address)
	{
		case VSYNC:
			//========================================================================
			// Vertical Sync
			if(!(value & 0x02))
			{
				self->myHBlankLines = 0;
			}
			break;

		case VBLANK:
			//========================================================================
			// VBLANK
			if(!(value & 0x02))
			{
				self->endFrame();
				self->startFrame();
				break;
			}

			if(value & 0x40)
			{
				self->myMemory[INPT4] |= 0x80;
				self->myMemory[INPT5] |= 0x80;
				break;
			}
			else
			{
				if(self->myJoystick0->isButtonPressed())
					self->myMemory[INPT4] &= 0x7f;
				else
					self->myMemory[INPT4] |= 0x80;

				if(self->myJoystick1->isButtonPressed())
					self->myMemory[INPT5] &= 0x7f;
				else
					self->myMemory[INPT5] |= 0x80;

				break;
			}

			//========================================================================
			// Dumped input ports (TOBE CONTINUED)
			if(value & 0x80)
			{
				self->myDumpedInputPortsFlag = true;
				self->myDumpedPort0Charge = 0;
				self->myDumpedPort1Charge = 0;
				self->myMemory[INPT0] &= 0x7f;
				self->myMemory[INPT1] &= 0x7f;
				self->myMemory[INPT2] &= 0x7f;
				self->myMemory[INPT3] &= 0x7f;
				break;
			}
			else if(!(value & 0x80))
			{
				self->myDumpedInputPortsFlag = false;
				break;
			}
			break;
		case WSYNC:
			self->update(228 - self->myCurrentScanlineClocks);
			break;

		case RSYNC:
			// for chip testing purposes only
			break;
		case NUSIZ0:
			self->myMissiles[0].scanSize = 1 << ((value & 0x30) >> 4);
			self->myMissiles[0].scanShift = 0;
			self->myPlayers[0].playerCopies = (PlayerCopies)(value & 0x07);
			switch(self->myPlayers[0].playerCopies)
			{
				case OneDoubleSizedCopy:
					self->myPlayers[0].scanShift = 1;
					self->myPlayers[0].scanSize = 16;
					break;
				case OneQuadSizedCopy:
					self->myPlayers[0].scanShift = 2;
					self->myPlayers[0].scanSize = 32;
					break;
				default:
					self->myPlayers[0].scanShift = 0;
					self->myPlayers[0].scanSize = 8;
					break;
			}
			break;
		case NUSIZ1:
			self->myMissiles[1].scanSize = 1 << ((value & 0x30) >> 4);
			self->myMissiles[1].scanShift = 0;
			self->myPlayers[1].playerCopies = (PlayerCopies)(value & 0x07);
			switch(self->myPlayers[1].playerCopies)
			{
				case OneDoubleSizedCopy:
					self->myPlayers[1].scanShift = 1;
					self->myPlayers[1].scanSize = 16;
					break;
				case OneQuadSizedCopy:
					self->myPlayers[1].scanShift = 2;
					self->myPlayers[1].scanSize = 32;
					break;
				default:
					self->myPlayers[1].scanShift = 0;
					self->myPlayers[1].scanSize = 8;
					break;
			}
			break;
		case COLUP0:
			self->myPlayers[0].color = (value & 0xf0) >> 4;
			self->myPlayers[0].luminosity = (value & 0x0e) >> 1;
			break;
		case COLUP1:
			self->myPlayers[1].color = (value & 0xf0) >> 4;
			self->myPlayers[1].luminosity = (value & 0x0e) >> 1;
			break;
		case COLUPF:
			self->myPlayfield.color = (value & 0xf0) >> 4;
			self->myPlayfield.luminosity = (value & 0x0e) >> 1;
			self->myBall.color = self->myPlayfield.color;
			self->myBall.luminosity = self->myPlayfield.luminosity;
			break;
		case COLUBK:
			self->myBackground.color = (value & 0xf0) >> 4;
			self->myBackground.luminosity = (value & 0x0e) >> 1;
			break;
		case CTRLPF:
			self->myBall.scanSize = 1 << ((value & 0x30) >> 4);
			self->myPlayfield.hasPriorityOverPlayers = (value & 0x04) >> 2;
			self->myPlayfield.score = (value & 0x02) >> 1;
			self->myPlayfield.reflect = (value & 0x01);
			break;
		case REFP0:
			self->myPlayers[0].reflect = (value & 0x08) >> 3;
			break;
		case REFP1:
			self->myPlayers[1].reflect = (value & 0x08) >> 3;
			break;
		//========================================================================
		// Playfield register
		// +-----------+-----------------------+-----------------------+
		// |    PF0    |          PF1          |        PF2            |
		// +-----------+-----------------------+-----------------------+
		// |d4 d5 d6 d7|d7 d6 d5 d4 d3 d2 d1 d0|d0 d1 d2 d3 d4 d5 d6 d7|
		// +-----------+-----------------------+-----------------------+
		case PF0:
			self->myPlayfield.playfieldBits &= ~0x000f0000;
			self->myPlayfield.playfieldBits |= ((value & 0x10) >> 4) << 19 |
																				 ((value & 0x20) >> 5) << 18 |
																				 ((value & 0x40) >> 6) << 17 |
																				 ((value & 0x80) >> 7) << 16 ;
			break;
		case PF1:
			self->myPlayfield.playfieldBits &= ~0x0000ff00;
			self->myPlayfield.playfieldBits |= value << 8;
			break;
		case PF2:
			self->myPlayfield.playfieldBits &= ~0x000000ff;
			self->myPlayfield.playfieldBits |= ((value & 1)  >> 0) << 7 |
																				 ((value & 2)  >> 1) << 6 |
																				 ((value & 4)  >> 2) << 5 |
																				 ((value & 8)  >> 3) << 4 |
																				 ((value & 16) >> 4) << 3 |
																				 ((value & 32) >> 5) << 2 |
																				 ((value & 64) >> 6) << 1 |
																				 ((value & 128)>> 7) << 0 ;
			break;

		//========================================================================
		// Reset Objects position
		// ----------------------
		// If the beam is inside HBLANK period the object must be reset to the
		// beggining of visible scanline
		//
		// This arrangement means that resetting the player counter on any
		// visible pixel will cause the main copy of the player to appear
		// at that same pixel position on the next and subsequent scanlines.
		// There are 5 CLK worth of clocking/latching to take into account,
		// so the actual position ends up 5 pixels to the right of the
		// reset pixel (approx. 9 pixels after the start of STA RESP0).
		case RESP0:
			self->resetObjectPosition(&self->myPlayers[0]);
			break;
		case RESP1:
			self->resetObjectPosition(&self->myPlayers[1]);
			break;
		case RESM0:
			self->resetObjectPosition(&self->myMissiles[0]);
			break;
		case RESM1:
			self->resetObjectPosition(&self->myMissiles[1]);
			break;
		case RESBL:
			self->resetObjectPosition(&self->myBall);
			break;
		case AUDC0:
			self->myAudioContent0 = (NoiseContent)(value & 0x0f);
			break;
		case AUDC1:
			self->myAudioContent1 = (NoiseContent)(value & 0x0f);
			break;
		case AUDF0:
			self->myAudioDivisor0 = value & 0x1f;
			break;
		case AUDF1:
			self->myAudioDivisor1 = value & 0x1f;
			break;
		case AUDV0:
			self->myAudioVolume0 = value & 0x0f;
			break;
		case AUDV1:
			self->myAudioVolume1 = value & 0x0f;
			break;

		/*
			Writes to GRP0 always modify the "new" P0 value, and the
			contents of the "new" P0 are copied into "old" P0 whenever
			GRP1 is written. (Likewise, writes to GRP1 always modify the
			"new" P1 value, and the contents of the "new" P1 are copied
			into "old" P1 whenever GRP0 is written). It is safe to modify
			GRPn at any time, with immediate effect.
		*/
		case GRP0:
			self->myPlayers[0].grp = value;
			self->myPlayers[1].oldGrp = self->myPlayers[1].grp;
			break;
		case GRP1:
			self->myPlayers[1].grp = value;
			self->myPlayers[0].oldGrp = self->myPlayers[0].grp;
			self->myBall.oldEnabled = self->myBall.enabled;
			break;
		case ENAM0:
			self->myMissiles[0].enabled = (value & 0x02) >> 1;
			break;
		case ENAM1:
			self->myMissiles[1].enabled = (value & 0x02) >> 1;
			break;
		case ENABL:
			self->myBall.enabled = (value & 0x02) >> 1;
			break;

		//========================================================================
		// Motion registers
		case HMP0:
			self->myPlayers[0].hmm = self->readHorizontalMovement(value);
			break;
		case HMP1:
			self->myPlayers[1].hmm = self->readHorizontalMovement(value);
			break;
		case HMM0:
			self->myMissiles[0].hmm = self->readHorizontalMovement(value);
			break;
		case HMM1:
			self->myMissiles[1].hmm = self->readHorizontalMovement(value);
			break;
		case HMBL:
			self->myBall.hmm = self->readHorizontalMovement(value);
			break;

		//========================================================================
		// Vertical Delay
		case VDELP0:
			self->myPlayers[0].verticalDelay = value & 0x01;
			break;
		case VDELP1:
			self->myPlayers[1].verticalDelay = value & 0x01;
			break;
		case VDELBL:
			self->myBall.verticalDelay = value & 0x01;
			break;

		//========================================================================
		// Reset missile to center of players
		case RESMP0:
			self->myMissiles[0].lockedOnPlayer = (value & 0x02) >> 1;
			break;
		case RESMP1:
			self->myMissiles[1].lockedOnPlayer = (value & 0x02) >> 1;
			break;

		//========================================================================
		// Act on the movement registers
		// -----------------------------
		case HMOVE:
			self->doHMove();
			break;

		//========================================================================
		// Clear movement registers
		case HMCLR:
			self->myPlayers[0].hmm = 0;
			self->myPlayers[1].hmm = 0;
			self->myMissiles[0].hmm = 0;
			self->myMissiles[1].hmm = 0;
			self->myBall.hmm = 0;
			break;

		//========================================================================
		// Clear collision state
		case CXCLR:
			self->clearCollisionLatches();
			break;
	}
	self->myMemory[address] = value;
}

void TIA::resetObjectPosition(MovableObject *object)
{
	object->resetWrittenFlag = object->position != (myCurrentScanlineClocks - 68);
	if(myCurrentScanlineClocks < 68)
		object->position = 2;
	else
		object->position = myCurrentScanlineClocks - 68 + 7;
}

void TIA::doHMove()
{
	if(myCurrentScanlineClocks < 68)
		myLateResetHBLANKFlag = true;

	myPlayers[0].position		-= myPlayers[0].hmm;
	myPlayers[1].position		-= myPlayers[1].hmm;
	myMissiles[0].position	-= myMissiles[0].hmm;
	myMissiles[0].position	-= myMissiles[0].hmm;
	myBall.position					-= myBall.hmm;

	wrapAround(&myPlayers[0].position);
	wrapAround(&myPlayers[1].position);
	wrapAround(&myMissiles[0].position);
	wrapAround(&myMissiles[1].position);
	wrapAround(&myBall.position);
}

void TIA::wrapAround(qint16* pos)
{
	if(*pos > 159) *pos -= 160;
	else if(*pos < 0) *pos += 160;
}

void TIA::setJoysticks(AtariJoystick *joystick0, AtariJoystick *joystick1)
{
	myJoystick0 = joystick0;
	myJoystick1 = joystick1;
}

void TIA::debug(const QString& debugMessage)
{
#ifdef DEBUGBUG
	qDebug("x: %i y:%i clks: %i player0: %i Msg: %s",	myCurrentScanlineClocks, myCurrentScanline,	myCurrentFrameClocks, myPlayers[0].positionCounter, debugMessage.toAscii().data());
#endif
}
