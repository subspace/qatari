/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef M6502_H
#define M6502_H

#include "M6502/M6502.h"
#include <QtGlobal>

class DataBus;

class M6502
{
public:
	/**
		Callback functions from the M6502 C source
	*/
	friend byte Rd6502(register word Addr);
	friend void Wr6502(register word Addr, register byte Value);
	friend byte Op6502(register word Addr);
	friend byte Patch6502(register byte Op, register _M6502 *R);
	//========================================================================

	static M6502*	globalInstance();
	bool					run();
	bool					reset();
	quint8				clocks() const { return myLastClocks; }
	void					setDataBus(DataBus *dataBus) { myDataBus = dataBus; }
	void					clearClocks() { myLastClocks = 0; }
	int						nextInstructionClocks();

private:
	M6502();
	~M6502();

	quint8				myLastClocks;

	static M6502*	ourInstance;
	_M6502*				myM6502;
	DataBus*			myDataBus;
};

#endif // M6502_H
