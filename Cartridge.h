/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef CARTRIDGE_H
#define CARTRIDGE_H

#include "DataBus.h"

class QString;
class QByteArray;
class DataBus;

class Cartridge
{
public:
	Cartridge();
	~Cartridge();

	bool loadCartridge(const QString &fileName);
	bool attach(DataBus *dataBus, Address start, Address end);

private:
	enum { MaxMemoryBanks = 8, MemoryBankSize = 0x1000 };

	DataBus			*myDataBus;
	QByteArray	*myRomData[MaxMemoryBanks];
	int					myNumberOfBanks;
	int					myCurrentBank;

	bool allocDataBanks(int count);
	void switchBanksIfNecessary(Address address);

	static Byte read(void *object, Address address);
	static void write(void *object, Address address, Byte value);
};

#endif // CARTRIDGE_H
