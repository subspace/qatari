/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "QtVideoRenderer.h"
#include <QImage>
#include <QVector>
#include <QRgb>
#include <QPixmap>
#include <QGraphicsScene>
#include <QGraphicsView>

/* TODO: Write better renderer after fixing all TIA bugs */

QtVideoRenderer::QtVideoRenderer():
TIAVideoRenderer(),
myImage(new QImage(320, 320, QImage::Format_RGB32))
{
}

QtVideoRenderer::~QtVideoRenderer()
{
	delete myImage;
}

void QtVideoRenderer::buildNTSCPalette()
{
	QVector<QRgb> colors;

	for(int i = 0; i < 128; ++i)
		colors.push_back(qRgb((NTSCColorTable[i * 2] & 0xff0000) >> 16, (NTSCColorTable[i * 2] & 0x00ff00) >> 8, (NTSCColorTable[i * 2] & 0x0000ff) >> 0));

	myImage->setColorTable(colors);
}

void QtVideoRenderer::buildPALPalette()
{
	QVector<QRgb> colors;

	for(int i = 0; i < 128; ++i)
		colors.push_back(qRgb((PALColorTable[i * 2] & 0xff0000) >> 16, (PALColorTable[i * 2] & 0x00ff00) >> 8, (PALColorTable[i * 2] & 0x0000ff) >> 0));

	myImage->setColorTable(colors);
}

//========================================================================
// drawPixel
// ---------------------------------------------
// This function draws the pixel with doubled width for a more realistic aspect
//
void QtVideoRenderer::drawPixel(int x, int y, quint8 color, quint8 luminosity)
{
	if(y > 320 || x > 160)
		return;
	x <<= 1;
	QRgb *scanline = (QRgb *)myImage->scanLine(y);
	scanline[x] = scanline[x+1] = myImage->color((color << 3 ) & 0x78 | luminosity & 0x07);
}

void QtVideoRenderer::startDrawing()
{
	for(int y = 0; y < 320; ++y)
	{
		QRgb* scanline = (QRgb*)myImage->scanLine(y);
		for(int x = 0; x < 320; ++x)
			scanline[x] = 0;
	}
}

void QtVideoRenderer::endDrawing()
{
	myScene->clear();
	QGraphicsPixmapItem *item = myScene->addPixmap(QPixmap::fromImage(*myImage));
	myView->update();
	countFPS();
}
