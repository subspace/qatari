/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

/**
	This class should take care of the memory mapping for all the emulator.
	It attaches the devices internal memory into the DataBus for reading and writing from
	other devices attached to the same DataBus.
	Avoiding virtual functions for speed sake
*/

#ifndef DATABUS_H
#define DATABUS_H

#include <QtGlobal>

#define THIS(class) (reinterpret_cast<class *>(object))

typedef uchar		Byte;
typedef quint16 Address;
typedef void (*DevicePin)(void *object, bool on);

class DataBus
{
private:
	typedef Byte (*ReadingFunction)(void *object, Address address);
	typedef void (*WritingFunction)(void *object, Address address, Byte value);

	struct MemoryRange {
		Address start, end;
	};

public:
	enum AccessType {ReadAccess, WriteAccess};

public:
	DataBus();

	bool attach(void *object, Address start, Address end, ReadingFunction readFunction, WritingFunction writeFunction);
	bool detach(void *object);
	AccessType lastAccessType() const { return myLastAccess; }
	Byte read(Address address);
	void write(Address address, Byte value);

private:
	/**
		The reason for the arrays of repeating objects is for speed purposes
		since this is a core component *very* used I decided to do it like that
		it wastes some memory but it's not a problem nowadays.
	*/
	AccessType			myLastAccess;
	ReadingFunction	myReadingFunctions[0x10000]; // 8K*4 = 16K
	WritingFunction	myWritingFunctions[0x10000]; // 8K*4 = 16K
	void						*myObjects[0x10000];				 // 8K*4 = 16K
	MemoryRange			myMemoryRanges[0x10000];		 // 8K*4 = 16K
																							 // 64K of memory on this thing, huge speed gain
	bool isAddressAttached(Address address);
};

#endif // DATABUS_H
