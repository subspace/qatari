/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef MACHINE_H
#define MACHINE_H

class M6502;
class M6532;
class TIA;
class Cartridge;
class DataBus;
class AtariJoystick;
class QString;
class TIAVideoRenderer;
class QTime;

class Machine
{
public:
	Machine();
	~Machine();

	bool insertCartridge(const QString &romPath);
	void reset(bool state);
	void select(bool state);
	void togglePlayerDifficulty(int num);
	void run();
	void toggleDebugMode();

	void setTIAVideoRenderer(TIAVideoRenderer *videoRenderer);

	AtariJoystick* joystick(int num);

private:
	void installM6502();
	void installM6532();
	void installTIA();
	void installCartridge();
	void installControllers();
	void readInput();

	M6502						*myM6502;
	M6532						*myM6532;
	TIA							*myTIA;
	Cartridge				*myCartridge;
	DataBus					*myDataBus;
	AtariJoystick		*myJoystick0, *myJoystick1;
	QTime						*myTime;
	bool						myDebugModeFlag;
	int							myLastFrameTime;
};

#endif // MACHINE_H
