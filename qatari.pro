#-------------------------------------------------
#
# Project created by QtCreator 2012-06-07T01:39:28
#
#-------------------------------------------------

QT       += core gui

TARGET = qatari

TEMPLATE = app

SOURCES += main.cpp \
		M6502.cpp \
    DataBus.cpp \
    Cartridge.cpp \
    M6532.cpp \
    Machine.cpp \
    TIA.cpp \
    TIAVideoRenderer.cpp \
    MainWindow.cpp \
    Controller.cpp \
    AtariJoystick.cpp \
    InputFilter.cpp \
    QtVideoRenderer.cpp \
    M6502/_M6502.c

HEADERS += \
		M6502.h \
    M6502/Tables.h \
    M6502/_M6502.h \
    M6502/Codes.h \
    DataBus.h \
    Cartridge.h \
    M6532.h \
    Machine.h \
    TIA.h \
    TIAVideoRenderer.h \
    MainWindow.h \
    Controller.h \
    AtariJoystick.h \
    InputFilter.h \
    QtVideoRenderer.h \
    M6502/M6502.h

FORMS += \
    MainWindow.ui
