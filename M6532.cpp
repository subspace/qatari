/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "M6532.h"

const quint8 M6532::ShiftTable[4] = { 0, 3, 6, 10 };

M6532::M6532():
myDataBus(NULL)
{
}

M6532::~M6532()
{
	if(myDataBus)
		myDataBus->detach(this);
}

void M6532::reset()
{
	for(Address i = 0; i < RamMemorySize; ++i)
		myRam[i] = 0;
	for(Address i = 0; i < PortsMemorySize; ++i)
		myPorts[i] = 0;

	// ppl say some roms halt if left 0
	myTimerShift = ShiftTable[qrand() & 3];
	myTimerClocks = qBound<int>(1, qrand() & 3, 4) << myTimerShift;

	myIRQEnabled = false;
	myIRQTriggered = false;

	myClocksSinceTimerStarted = 0;
}

void M6532::run(int clks)
{
	myClocksSinceTimerStarted += clks;
}

bool M6532::attachRam(Address start, Address end)
{
	if(!myDataBus)
		return false;
	if((end - start)+1 != RamMemorySize)
		return false;

	return myDataBus->attach(this, start, end, M6532::readRam, M6532::writeRam);
}

bool M6532::attachPortsAndTimer(Address start, Address end)
{
	if(!myDataBus)
		return false;
	if((end-start)+1 != PortsMemorySize)
		return false;

	return myDataBus->attach(this, start, end, M6532::readPort, M6532::writePort);
}

//========================================================================

Byte M6532::readRam(void *object, Address address)
{
	return THIS(M6532)->myRam[address];
}

void M6532::writeRam(void *object, Address address, Byte value)
{
	THIS(M6532)->myRam[address] = value;
}

Byte M6532::readPort(void *object, Address address)
{
	M6532 *self = THIS(M6532);

	if(address == INTIM || address == 0x06)
	{
		self->myIRQTriggered = false;
		qint32 timerValue = self->myTimerClocks - self->myClocksSinceTimerStarted;
		if(timerValue >= 0)
			return (timerValue >> self->myTimerShift) & 0xff;
		else
		{
			if(timerValue != -1)
				self->myIRQTriggered = true;

			return timerValue & 0xff;
		}
	}

	// read interrupt
	if(address == 0x05 || address == 0x07)
	{
		qint32 timerValue = self->myTimerClocks - self->myClocksSinceTimerStarted;
		if(timerValue >= 0 || (self->myIRQEnabled && self->myIRQTriggered))
			return 0x00;
		else
			return 0x80;
	}

	return self->myPorts[address];
}

void M6532::writePort(void *object, Address address, Byte value)
{
	M6532 *self;

	self = THIS(M6532);
	self->myPorts[address] = value;

	if(address & 0x04)
	{
		if(address & 0x10)
		{
			self->myIRQEnabled = (address & 0x08)	!= 0;
			self->myTimerShift = ShiftTable[address & 0x03];
			self->myTimerClocks = value << self->myTimerShift;
			self->myClocksSinceTimerStarted = 0;
		}
	}
}
