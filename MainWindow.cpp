/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Machine.h"
#include "QtVideoRenderer.h"
#include "InputFilter.h"
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	myVideoRenderer = new QtVideoRenderer();
	myGraphicsScene = new QGraphicsScene(this);
	myVideoRenderer->setScene(myGraphicsScene);
	myVideoRenderer->setView(ui->graphicsView);
	ui->graphicsView->setScene(myGraphicsScene);

	myAtari = new Machine();
	myAtari->setTIAVideoRenderer(myVideoRenderer);
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Boxing (1).bin"); // PAL
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Alien.bin"); // NTSC
	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Ms. Pac-Man.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Treasure Below (Video Gems, Thomas Jentzsch) (NTSC).bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Pac-Man.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Pac-Man (hack).BIN");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Action Man.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Duck Shoot (PAL).bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/kernel_15.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/kernel_11.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/kernel_13.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/kernel22.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Dragonfire.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Fighterp.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/Nexar.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/EarlyHMOVE.bin");
//	myAtari->insertCartridge(QApplication::applicationDirPath() + "/roms/testcart.bin");

	InputFilter *myInputFilter = new InputFilter(myAtari, this);
	installEventFilter(myInputFilter);
	on_actionResume_triggered();
}

void MainWindow::timerEvent(QTimerEvent *)
{
	static int oldFPS = myVideoRenderer->framesPerSecond();

	myAtari->run();

	if(oldFPS != myVideoRenderer->framesPerSecond())
	{
		qDebug("%i", myVideoRenderer->framesPerSecond());
		oldFPS = myVideoRenderer->framesPerSecond();
	}
}

void MainWindow::on_actionResume_triggered()
{
	killTimer(myTimerID);
	myTimerID = startTimer(0);
}

void MainWindow::on_action_Open_ROM_triggered()
{
	killTimer(myTimerID);
	QString filename = QFileDialog::getOpenFileName(this, "Select a ROM...", QString());
	if(filename.isEmpty())
		return;
	myAtari->insertCartridge(filename);
	myTimerID = startTimer(0);
}

void MainWindow::on_actionStop_triggered()
{
	killTimer(myTimerID);
}

void MainWindow::on_menu_About_triggered()
{
  QMessageBox::information(this, "About...", "QAtari v0.01 by Washu");
}

void MainWindow::on_action_Exit_triggered()
{
	close();
}

MainWindow::~MainWindow()
{
	delete ui;
}
