/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef QTVIDEORENDERER_H
#define QTVIDEORENDERER_H

#include "TIAVideoRenderer.h"

class QImage;
class QGraphicsScene;
class QGraphicsView;

class QtVideoRenderer: public TIAVideoRenderer
{
public:
	QtVideoRenderer();
	~QtVideoRenderer();
	void setScene(QGraphicsScene* scene) { myScene = scene; }
	void setView(QGraphicsView* view) { myView = view; }

	void startDrawing();
	void endDrawing();
	void drawPixel(int x, int y, quint8 color, quint8 luminosity);
	void buildNTSCPalette();
	void buildPALPalette();

private:
	QImage*					myImage;
	QGraphicsScene* myScene;
	QGraphicsView*	myView;
};

#endif // QTVIDEORENDERER_H
