/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "Machine.h"
#include "M6502.h"
#include "M6532.h"
#include "TIA.h"
#include "Cartridge.h"
#include "AtariJoystick.h"
#include <QTime>
#include <QMutex>
#include <QWaitCondition>
#include <QDebug>
#include <QThread>

class Sleeper: public QThread
{
public:
	static void msleep(unsigned long msec)
	{
		QThread::msleep(msec);
	}
};

Machine::Machine():
myM6502(M6502::globalInstance()),
myM6532(new M6532),
myTIA(new TIA),
myCartridge(new Cartridge),
myDataBus(new DataBus),
myTime(new QTime),
myDebugModeFlag(false),
myLastFrameTime(0)
{
	installTIA();
	installCartridge();
	installControllers();
	installM6502();
	installM6532();
}

Machine::~Machine()
{
	delete myM6532;
	delete myTIA;
	delete myCartridge;
	delete myDataBus;
	delete myTime;
}

void Machine::installControllers()
{
	myJoystick0 = new AtariJoystick(0);
	myJoystick1 = new AtariJoystick(1);
	myTIA->setJoysticks(myJoystick0, myJoystick1); // for reading the fire button
}

void Machine::toggleDebugMode()
{
	myDebugModeFlag = !myDebugModeFlag;
}

bool Machine::insertCartridge(const QString &romPath)
{
	bool r = myCartridge->loadCartridge(romPath);
	if(!r)
		return false;

	myM6532->reset();
	myM6502->reset();
	myTIA->reset();

	return true;
}

void Machine::run()
{
	int frameRate;

	/* Hold frame rate (FIXME: make do it only once) */
	if(myTIA->televisionSystem() == TIA::NtscSystem)
		frameRate = 60;
	else // secam/pal
		frameRate = 50;

	if(myDebugModeFlag)
		frameRate = 5;

	if(myTime->elapsed() < (1000.0f/frameRate - myLastFrameTime - 1))
	{
		Sleeper::msleep(1);
		return;
	}

	myTime->restart();

	// Do a complete screen frame
	while(!myTIA->currentFrameFinished())
	{
		int clks = myM6502->nextInstructionClocks();
		myTIA->run(clks * 3);
		myM6532->run(clks);
		myM6502->run();

		readInput();
	}

	myLastFrameTime = myTime->restart();
}

void Machine::reset(bool state)
{
	Byte swchb = myDataBus->read(0x282);
	if(state)
		swchb &= ~0x01;
	else
		swchb |= 0x01;
	myDataBus->write(0x282, swchb);
	qDebug("reset");
}

void Machine::select(bool state)
{
	Byte swchb = myDataBus->read(0x0282);
	swchb = state ? swchb & ~0x02 : swchb | 0x02;
	myDataBus->write(0x0282, swchb);
	qDebug("select");
}

void Machine::togglePlayerDifficulty(int num)
{
	Byte swchb = myDataBus->read(0x0282);

	num = qBound<int>(0, num, 1);

	if(num == 0)
		swchb ^= 0x40;
	else
		swchb ^= 0x80;

	myDataBus->write(0x282, swchb);
}

void Machine::setTIAVideoRenderer(TIAVideoRenderer *videoRenderer)
{
	if(myTIA)
		myTIA->setVideoRenderer(videoRenderer);
}

AtariJoystick* Machine::joystick(int num)
{
	num = qBound<int>(0, num, 1);
	if(!num)
		return myJoystick0;
	else
		return myJoystick1;
}

void Machine::readInput()
{
	Byte switches;

	switches = myJoystick0->switchesState();
	switches |= myJoystick1->switchesState();
	myDataBus->write(0x281, 0);
	myDataBus->write(0x280, switches);
}

//========================================================================

void Machine::installM6502()
{
	myM6502->setDataBus(myDataBus);
}

void Machine::installTIA()
{
	for(quint32 i = 0x0000; i < 0xEF80; i += (1<<6))
		myTIA->attach(myDataBus, i, i + (1<<6)-1);
}

void Machine::installCartridge()
{
	for(quint32 i = 0x1000; i < 0xFFFF; i += (1<<13))
		myCartridge->attach(myDataBus, i, i + (1<<12) - 1);
}

void Machine::installM6532()
{
	myM6532->setDataBus(myDataBus);

	for(quint32 i = 0x280, j = 0; i < 0xEFFF; i += ((++j % 4) == 0 ? 160 : 32) + ((j % 8) == 0 ? 512 : 0))
	{
		j=j;
		myM6532->attachPortsAndTimer(i, i + 31);

		if((i & 0x0FE0) == 0x0FE0)
			i += 0x1000;
	}

	for(quint32 i = 0x0080, j = 0; i < 0xEDFF; i += (++j & 1) == 0 ? 768 : 256)
	{
		myM6532->attachRam(i, i + 127);
		if((i & 0x0D80) == 0x0D80)
			i += 0x1000;
	}
}
