/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "M6502.h"
#include "DataBus.h"
#include <memory.h>
#include <stdio.h>

M6502			*M6502::ourInstance;

byte Rd6502(register word Addr)
{
	return M6502::ourInstance->myDataBus->read(Addr);
}

void Wr6502(register word Addr, register byte Value)
{
	M6502::ourInstance->myDataBus->write(Addr, Value);
}

byte Op6502(register word Addr)
{
	return M6502::ourInstance->myDataBus->read(Addr);
}

byte Patch6502(register byte Op, register _M6502 *R)
{
	Q_UNUSED(Op)
	Q_UNUSED(R)
	return 0;
}

//========================================================================

M6502::M6502():
myM6502(new _M6502),
myDataBus(NULL)
{
	ourInstance = NULL;
}

M6502::~M6502()
{
	delete myM6502;
}

M6502 *M6502::globalInstance()
{
	if(!ourInstance)
		ourInstance = new M6502();
	return ourInstance;
}

bool M6502::run()
{
	if(!myDataBus)
		return false;

	myLastClocks = nextInstructionClocks();
	Exec6502(myM6502, 1);

	return true;
}

int M6502::nextInstructionClocks()
{
	return 1 - GetNextInstructionClocks(myM6502, 1);
}

bool M6502::reset()
{
	if(!myDataBus)
		return false;

	memset(myM6502, 0, sizeof(_M6502));
	Reset6502(myM6502);
	myLastClocks = 0;

	return true;
}
