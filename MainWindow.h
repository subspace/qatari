/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class Machine;
class QtVideoRenderer;
class QGraphicsScene;

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void on_action_Open_ROM_triggered();
	void on_actionResume_triggered();
	void on_actionStop_triggered();
	void on_action_Exit_triggered();
	void on_menu_About_triggered();

protected:
	void timerEvent(QTimerEvent *);

private:
	Ui::MainWindow		*ui;
	QtVideoRenderer		*myVideoRenderer;
	QGraphicsScene		*myGraphicsScene;
	Machine						*myAtari;
	int								myTimerID;
};

#endif // MAINWINDOW_H
