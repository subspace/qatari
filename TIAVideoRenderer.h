/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef TIAVIDEORENDERER_H
#define TIAVIDEORENDERER_H

#include <QtGlobal>
class QTime;

class TIAVideoRenderer
{
public:
	explicit TIAVideoRenderer();
	virtual ~TIAVideoRenderer();

	//========================================================================
	// Those functions are called from the TIA and must be implemented
	virtual void startDrawing() = 0;
	virtual void endDrawing() = 0;
	virtual void drawPixel(int x, int y, quint8 color, quint8 luminosity) = 0;
	virtual void buildNTSCPalette() = 0;
	virtual void buildPALPalette() = 0;

	//========================================================================
	// Returns the number of frames per second that are being rendered by the TIA
	int framesPerSecond() const { return myFramesPerSecond; }

protected:
	//========================================================================
	// FPS Counter -> must be called everyframe ending by the user on endDrawing
	// otherwise framesPerSecond function will return zit
	void countFPS();

		struct Color {
			quint8 r, g, b;
		};
		static const int NTSCColorTable[256];
		static const int PALColorTable[256];
		static const int SECAMColorTable[256];

private:
		int			myFramesPerSecond;
		QTime*	myTime;
};

#endif // TIAVIDEORENDERER_H
