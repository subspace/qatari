/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "DataBus.h"

class Controller
{
public:
	Controller();

	inline void moveLeft()
	{
		myLeftPressed = true;
		releaseRight();
	}

	inline void moveRight()
	{
		myRightPressed = true;
		releaseLeft();
	}

	inline void moveUp()
	{
		myUpPressed = true;
		releaseDown();
	}

	inline void moveDown()
	{
		myDownPressed = true;
		releaseUp();
	}

	inline void releaseUp()
	{
		myUpPressed = false;
	}
	inline void releaseDown()
	{
		myDownPressed = false;
	}
	inline void releaseLeft()
	{
		myLeftPressed = false;
	}
	inline void releaseRight()
	{
		myRightPressed = false;
	}

	inline void moveNone()
	{
		qDebug("release");
		myDownPressed = myUpPressed = myLeftPressed = myRightPressed = false;
	}

	inline void pressButton()
	{
		qDebug("button");
		myButtonPressed = true;
	}

	inline void releaseButton()
	{
		qDebug("releasebutton");
		myButtonPressed = false;
	}

	inline bool isLeftPressed() const { return myLeftPressed; }
	inline bool isRightPressed() const { return myRightPressed; }
	inline bool isUpPressed() const { return myUpPressed; }
	inline bool isDownPressed() const { return myDownPressed; }
	inline bool isButtonPressed() const { return myButtonPressed; }

private:
	bool myUpPressed, myDownPressed, myLeftPressed, myRightPressed, myButtonPressed;
};

#endif // CONTROLLER_H
