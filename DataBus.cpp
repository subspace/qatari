/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "DataBus.h"

DataBus::DataBus()
{
	for(Address i = 0; i < 0xffff; ++i)
	{
		myMemoryRanges[i].start = myMemoryRanges[i].end = 0;
		myReadingFunctions[i] = NULL;
		myWritingFunctions[i] = NULL;
		myObjects[i] = NULL;
	}
}

bool DataBus::isAddressAttached(Address address)
{
	return myObjects[address] ? true : false;
}

bool DataBus::attach(void *object, Address start, Address end, ReadingFunction readFunction, WritingFunction writeFunction)
{
	for(int i = start; i <= end; ++i)
	{
		myMemoryRanges[i].start = start;
		myMemoryRanges[i].end = end;
		myReadingFunctions[i] = readFunction;
		myWritingFunctions[i] = writeFunction;
		myObjects[i] = object;
	}
	return true;
}

bool DataBus::detach(void *object)
{
	bool found = false;
	for(Address i = 0; i < 0xffff; ++i)
	{
		if(myObjects[i] == object)
		{
			found = true;
			myMemoryRanges[i].start = myMemoryRanges[i].end = 0;
			myReadingFunctions[i] = NULL;
			myWritingFunctions[i] = NULL;
			myObjects[i] = NULL;
		}
	}
	return found;
}

Byte DataBus::read(Address address)
{
	if(!isAddressAttached(address))
	{
		throw "Reading unattached address";
	}
	myLastAccess = ReadAccess;

	return myReadingFunctions[address](myObjects[address], address - myMemoryRanges[address].start);
}

void DataBus::write(Address address, Byte value)
{
	if(!isAddressAttached(address))
	{
		throw "Writing to unattached address";
	}
	myLastAccess = WriteAccess;

	return myWritingFunctions[address](myObjects[address], address - myMemoryRanges[address].start, value);
}
