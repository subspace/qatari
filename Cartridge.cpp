/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "Cartridge.h"

#include <QString>
#include <QFile>

Cartridge::Cartridge():
myDataBus(NULL),
myNumberOfBanks(0),
myCurrentBank(0)
{
	for(int i = 0; i < MaxMemoryBanks; ++i)
	{
		myRomData[i] = NULL;
	}
}

bool Cartridge::allocDataBanks(int count)
{
	int i;

	if(count > MaxMemoryBanks || count < 0)
	{
		for(i = 0; i < myNumberOfBanks; ++i)
		{
			delete myRomData[i];
		}
		myNumberOfBanks = 0;
		return false;
	}

	// clear the memory banks, they are expected to be all clean after this function
	for(i = 0; i < myNumberOfBanks; ++i)
	{
		myRomData[i]->clear();
	}

	if(count == myNumberOfBanks) //already there
		return true;

	if(count > myNumberOfBanks)
	{
		int howManyNewBanks = count - myNumberOfBanks;
		for(i = 0; i < howManyNewBanks; ++i)
		{
			myRomData[myNumberOfBanks + i] = new QByteArray();
		}
		myNumberOfBanks += i;
		return true;
	}

	if(count < myNumberOfBanks)
	{
		int banksToDelete = myNumberOfBanks - count;
		for(i = 0; i < banksToDelete; ++i)
		{
			delete myRomData[myNumberOfBanks - i];
		}
		myNumberOfBanks -= i;
	}
	return true;
}

bool Cartridge::loadCartridge(const QString &fileName)
{
	if(!myDataBus)
		return false;

	QFile romFile(fileName);
	romFile.open(QFile::ReadOnly);
	if(!romFile.isOpen())
		return false;

	QByteArray romData = romFile.readAll();

	// 4k simple cartridge 1 bank
	if(romData.size() == 4096)
	{
		if(!allocDataBanks(1))
			return false;
		myRomData[0]->append(romData);
	}
	// 2k cart mirror it twice in one bank
	else if(romData.size() == 2048)
	{
		if(!allocDataBanks(1))
			return false;
		myRomData[0]->append(romData);
		myRomData[0]->append(romData);
	}
	// 8k rom 2 banks
	else if(romData.size() == 8192)
	{
		if(!allocDataBanks(2))
			return false;
		*myRomData[0] = romData.left(4096);
		*myRomData[1] = romData.right(4096);
	}
	// 12k rom 3 banks
	else if(romData.size() == 12288)
	{
		if(!allocDataBanks(3))
			return false;
		*myRomData[0] = romData.mid(0, 4096);
		*myRomData[1] = romData.mid(4096, 4096);
		*myRomData[2] = romData.mid(8192, 4096);
	}
	// 16k rom 4 banks
	else if(romData.size() == 16384)
	{
		if(!allocDataBanks(4))
			return false;
		*myRomData[0] = romData.mid(0, 4096);
		*myRomData[1] = romData.mid(4096, 4096);
		*myRomData[2] = romData.mid(8192, 4096);
		*myRomData[3] = romData.mid(12288, 4096);
	}
	// 32k rom 8 banks
	else if(romData.size() == 32768)
	{
		if(!allocDataBanks(MaxMemoryBanks))
			return false;
		*myRomData[0] = romData.mid(0, 4096);
		*myRomData[1] = romData.mid(4096, 4096);
		*myRomData[2] = romData.mid(8192, 4096);
		*myRomData[3] = romData.mid(12288, 4096);
		*myRomData[4] = romData.mid(16384, 4096);
		*myRomData[5] = romData.mid(20480, 4096);
		*myRomData[6] = romData.mid(24576, 4096);
		*myRomData[7] = romData.mid(28672, 4096);
	}
	myCurrentBank = 0;

	return true;
}

bool Cartridge::attach(DataBus *dataBus, Address start, Address end)
{
	if((end-start)+1 != MemoryBankSize)
		return false;

	myDataBus = dataBus;
	return dataBus->attach(this, start, end, Cartridge::read, Cartridge::write);
}

Byte Cartridge::read(void *object, Address address)
{
	Cartridge *self = reinterpret_cast<Cartridge *>(object);
	Byte data = self->myRomData[self->myCurrentBank]->data()[address];

	self->switchBanksIfNecessary(address);

	return data;
}

void Cartridge::write(void *object, Address address, Byte value)
{
	Cartridge *self = reinterpret_cast<Cartridge *>(object);

	self->myRomData[self->myCurrentBank]->data()[address] = value;
	self->switchBanksIfNecessary(address);
}

void Cartridge::switchBanksIfNecessary(Address address)
{
	switch(myNumberOfBanks)
	{
		case 1:
			return;
		case 2:
			switch(address)
			{
				case 0xff8:
					myCurrentBank = 0;
					return;
				case 0xff9:
					myCurrentBank = 1;
					return;
			}
			return;
		case 3:
			switch(address)
			{
				case 0xff8:
					myCurrentBank = 0;
					return;
				case 0xff9:
					myCurrentBank = 1;
					return;
				case 0xffa:
					myCurrentBank = 2;
					return;
			}
			return;
		case 4:
			switch(address)
			{
				case 0xff6:
					myCurrentBank = 0;
					return;
				case 0xff7:
					myCurrentBank = 1;
					return;
				case 0xff8:
					myCurrentBank = 2;
					return;
				case 0xff9:
					myCurrentBank = 3;
					return;
			}
			return;
		case 8:
			switch(address)
			{
				case 0xff4:
					myCurrentBank = 0;
					return;
				case 0xff5:
					myCurrentBank = 1;
					return;
				case 0xff6:
					myCurrentBank = 2;
					return;
				case 0xff7:
					myCurrentBank = 3;
					return;
				case 0xff8:
					myCurrentBank = 4;
					return;
				case 0xff9:
					myCurrentBank = 5;
					return;
				case 0xffa:
					myCurrentBank = 6;
					return;
				case 0xffb:
					myCurrentBank = 7;
					return;
			}
			return;
		default:
			return;
	}
}

Cartridge::~Cartridge()
{
	delete myRomData;
	if(!myDataBus)
		myDataBus->detach(this);
}
