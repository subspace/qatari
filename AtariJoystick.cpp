/*
GNU General Public License version 3 notice

Copyright (C) 2012 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "AtariJoystick.h"

AtariJoystick::AtariJoystick(int num): Controller(),
myControllerNumber(qBound<int>(0, num, 1))
{
}

quint8 AtariJoystick::switchesState()
{
	quint8 switches = 0xff;

	if(isUpPressed())
		switches &= ~0x10;
	else if(isDownPressed())
		switches &= ~0x20;

	if(isLeftPressed())
		switches &= ~0x40;
	else if(isRightPressed())
		switches &= ~0x80;

	if(myControllerNumber)
		switches >>= 4;

	return switches;
}
